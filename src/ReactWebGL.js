/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is the main ReactWebGL module.
 *
 * This library borrows ideas and patterns from react-canvas (https://github.com/Flipboard/react-canvas) and
 * ReactART (https://github.com/reactjs/react-art).
 *
 * @providesModule ReactWebGL
 * @version 1.0
 * @author albertwang, TCSASSEMBLER
 */

'use strict';

/**
 * This defines the main ReactWebGL module, which exports all React components in the library.
 *
 * @type {{WebGL: (WebGL|exports), Quad: (Quad|exports)}}
 */
var ReactWebGL = {
    WebGL : require('./WebGL'),
    Quad : require('./Quad'),
    Image: require('./Image'),
    Text: require('./Text')
};

module.exports = ReactWebGL;