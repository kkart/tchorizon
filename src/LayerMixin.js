/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * LayerMixin provides common operations of a layer.
 *
 * Adapted from react-canvas (https://github.com/Flipboard/react-canvas).
 *
 * @providesModule LayerMixin
 * @version 1.0
 * @author react-canvas(https://github.com/Flipboard/react-canvas), albertwang
 */
'use strict';

var FrameUtils = require('./FrameUtils');
var DrawingUtils = require('./DrawingUtils');

// Declare the mixin
var LayerMixin = {
    /**
     * This is called to construct a layer from an element.
     * @param element
     */
    construct: function (element) {
        this._currentElement = element;
    },

    /**
     * This method is called to apply layer properties.
     * @param prevProps the previous properties
     * @param props the new properties
     */
    applyLayerProps: function (prevProps, props) {
        var layer = this.node;
        layer.backgroundColor = DrawingUtils.parseRGBAColor (props.backgroundColor || 'rgba(0, 0, 0, 1)');
        layer.width = props.width || 0;
        layer.height = props.height || 0;
        layer.x = props.x || 0;
        layer.y = props.y || 0;
        layer.zIndex = props.zIndex;
        layer.frame = FrameUtils.make(layer.x, layer.y, layer.width, layer.height);
        layer.id = props.id;
    },

    /**
     * This method is called before the component is mounted to a node, it is overridden to throw error because
     * a layer cannot be mounted directly.
     * @param rootID the root ID
     * @param container the container
     */
    mountComponentIntoNode: function (rootID, container) {
        throw new Error(
            'You cannot render this component standalone. ' +
            'You need to wrap it in a WebGL.'
        );
    }
};

module.exports = LayerMixin;
