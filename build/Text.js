/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is the Text react component implementation.
 *
 * @providesModule Text
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

var React = require('react');

var QuadMixin = require('./QuadMixin');
var LayerMixin = require('./LayerMixin');
var createComponent = require('./createComponent');
var DrawingUtils = require('./DrawingUtils');

/**
 * This function is used to draw the text component.
 * @private
 * @param {WebGLRenderingContext) ctx The WebGL context.
 * @param {number} translatedX The translated x position
 * @param {number} translatedY The translated y position
 */
function _drawTextRenderLayer (ctx, translatedX, translatedY) {

    if (null == this.texture) {
        this.texture = DrawingUtils.createTextTexture(ctx, this.width, this.height, 
            this.text, this.fontOptions);
    }

	if (this.texture) {
    	DrawingUtils.drawTexture(ctx, translatedX, translatedY, this.width, 
    		this.height, this.texture);
	}
}

/**
 * This function is used to get text from the children of the Text component.
 * @private
 * @param children The children of the Text component.
 * @return the text
 */
function _childrenAsString(children) {
    if (!children) {
        return '';
    }
    if (typeof children === 'string') {
        return children;
    }
    if (children.length) {
        return children.join('\n');
    }
    return '';
}

// Declare the Text component
var Text = createComponent('Text', LayerMixin, QuadMixin, {
    /**
     * Represents the property types.
     */
	propTypes: {
	    fontColor: React.PropTypes.string,
        fontSize: React.PropTypes.string,
        fontStyle: React.PropTypes.string,
        fontWeight: React.PropTypes.string,
        lineHeight: React.PropTypes.string,
        fontFamily: React.PropTypes.string
	},

    /**
     * Returns the default properties.
     */
    getDefaultProps: function() {
        return {
            fontColor: 'rgba(0, 0, 0, 1)',
            fontSize: '12px',
            fontStyle: 'normal',
            fontWeight: 'normal',
            lineHeight: '16px',
            fontFamily: 'Arial, Sans-serif'
        }
    },

  	/**
     * This method is called to apply the component properties.
     * @param prevProps the previous properties
     * @param props the new properties
     */
    applyComponentProps: function (prevProps, props) {
        this.applyLayerProps(prevProps, props);

        var text = _childrenAsString(props.children);
        var defaultProps = this.getDefaultProps();

        var fontOptions = {};
        fontOptions.fontColor = props.fontColor || defaultProps.fontColor;
        fontOptions.fontSize = props.fontSize || defaultProps.fontSize;
        fontOptions.fontStyle = props.fontStyle || defaultProps.fontStyle;
        fontOptions.fontWeight = props.fontWeight || defaultProps.fontWeight;
        fontOptions.lineHeight = props.lineHeight || defaultProps.lineHeight;
        fontOptions.fontFamily = props.fontFamily || defaultProps.fontFamily;
        
        this.node.text = text;
        this.node.fontOptions = fontOptions;
        this.node.texture = null;

        this.node.drawRenderLayer = _drawTextRenderLayer;
    },

    /**
     * This method is called to mount the component.
     * @param rootID the root ID
     * @param transaction the reconcile transaction
     * @param context the context
     * @returns {RenderLayer} the mounted render layer
     */
    mountComponent: function (rootID, transaction, context) {
        var props = this._currentElement.props;
        var layer = this.node;

        this.applyComponentProps({}, props);

        return layer;
    },

    /**
     * This method is called when a component is received.
     * @param nextComponent the next component
     * @param transaction the reconcile transaction
     * @param context the context
     */
    receiveComponent: function (nextComponent, transaction, context) {
        var props = nextComponent.props;
        var prevProps = this._currentElement.props;
        this.applyComponentProps(prevProps, props);
        this._currentElement = nextComponent;
    },

    /**
     * This method is called to un-mount the component.
     */
    unmountComponent: function () {
        LayerMixin.unmountComponent.call(this);
    }

});

module.exports = Text;